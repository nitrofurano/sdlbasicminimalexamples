
Minimal examples are excellent for learning any kind of programming language (version from Paulo Silva).

The purpose of this minimal example collection is to provide an example for each available sdlBasic command, specially for didactically helping newbies on doing their first codes, and progressivelly developing their ideas easier, since this method seems (if well coded for it) to show clearly how each command works.

This collection also aims on helping sdlBasic popularity.

For now, only 216 commands have minimal examples here (until now there are 457 commands) - more than 45%...

Any suggestion and help is welcome - updating sdlBasic forum and files pages, publishing on your own personal pages, or contacting anyone of us from the sdlBasic development staff, or any other way you think can be useful too.

The 'Done' folder contains the examples i'm counting as ready.

The 'ToDo' folder may have:
- empty examples (zero byte sized ones, the most common situation)
- non-minimal examples
- non-working examples
- non-tested examples

Very thanks to Zoiba and Vroby for motivation and helping!
